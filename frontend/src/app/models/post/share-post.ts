export interface SharePost {
    postId: number;
    email: string;
}