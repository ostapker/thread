export interface UpdatePost {
    id: number;
    body: string;
    previewImage: string;
}