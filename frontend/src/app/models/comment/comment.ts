import { User } from '../user';
import { Reaction } from '../reactions/reaction';
import { Post } from '../post/post';

export interface Comment {
    id: number;
    createdAt: Date;
    author: User;
    body: string;
    postId: number;
    reactions: Reaction[];
}
