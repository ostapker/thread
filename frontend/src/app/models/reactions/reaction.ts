import { User } from '../user';

export interface Reaction {
    isReaction: boolean;
    user: User;
}
