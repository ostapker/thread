export interface NewReaction {
    entityId: number;
    isReaction: boolean;
    userId: number;
}
