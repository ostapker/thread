import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { CommentService } from 'src/app/services/comment.service';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';
import { Subject, empty, Observable } from 'rxjs';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { UpdateComment } from 'src/app/models/comment/update-comment';
import { MatDialog } from '@angular/material/dialog';
import { StatisticsDialogComponent } from '../statistics-dialog/statistics-dialog.component';
import { AuthenticationService } from 'src/app/services/auth.service';
import { LikeService } from 'src/app/services/like.service';
import { DisLikeService } from 'src/app/services/dislike.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { MainThreadComponent } from '../main-thread/main-thread.component';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public reactionHub: HubConnection;

    @Output() deleteCommentEvent: EventEmitter<number> = new EventEmitter();
    @Output() updateCommentEvent: EventEmitter<Comment> = new EventEmitter();

    private unsubscribe$ = new Subject<void>();
    public updatedComment = {} as UpdateComment;

    public isEditing = false;

    public likesCount = 0;
    public dislikesCount = 0;
    public likeColor = 'primary';
    public dislikeColor = 'primary';

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private dialog: MatDialog,
        private likeService: LikeService,
        private dislikeService: DisLikeService,

    ) { }

    public ngOnInit() {
        this.updatedComment.id = this.comment.id;
        this.updatedComment.body = this.comment.body;
        this.registerHub();
        this.reactionsCalculate();
    }

    public registerHub() {
        this.reactionHub.on('CommentReactions', (response) => {
            if (response) {
                this.updateCommentReactions(response);
            }
        });
    }

    public updateCommentReactions(response: any) {
        console.log("comment");
        if (response.commentId === this.comment.id) {
            this.reactionsCalculate();
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public canEdit() {
        if (this.currentUser != null && this.comment.author.id == this.currentUser.id)
            return true;

        return false;
    }

    public editComment() {
        this.isEditing = true;
    }

    public close() {
        this.isEditing = false;
    }

    public save() {
        this.commentService
            .updateComment(this.updatedComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.updateCommentEvent.emit(resp.body)
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        this.isEditing = false;
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.deleteCommentEvent.emit(this.comment.id);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public reactionsCalculate() {
        this.likesCount = this.comment.reactions.filter(reaction => reaction.isReaction == true).length;
        this.dislikesCount = this.comment.reactions.filter(reaction => reaction.isReaction == false).length;

        this.likeColor = 'primary';
        this.dislikeColor = 'primary';

        if (this.currentUser != null) {
            var myReaction = this.comment.reactions.filter((x) => x.user.id === this.currentUser.id)[0];

            if (myReaction != null) {
                if (myReaction.isReaction)
                    this.likeColor = 'accent';
                if (!myReaction.isReaction)
                    this.dislikeColor = 'warn';
            }
        }
    }

    public showStatistics() {
        const dialog = this.dialog.open(StatisticsDialogComponent, {
            data: { reactions: this.comment.reactions },
            minWidth: 250,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.dislikeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.dislikeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

}
