import { Component, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { DisLikeService } from '../../services/dislike.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { MainThreadComponent } from '../main-thread/main-thread.component'
import { MatDialog } from '@angular/material/dialog';
import { EditPostDialogComponent } from '../../components/edit-post-dialog/edit-post-dialog.component';
import { StatisticsDialogComponent } from '../statistics-dialog/statistics-dialog.component';
import { ɵEmptyOutletComponent } from '@angular/router';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Reaction } from 'src/app/models/reactions/reaction';
import { SharePost } from 'src/app/models/post/share-post';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit, OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public postHub = this.mainThreadComponent.postHub;
    public commentHub: HubConnection;
    public reactionHub: HubConnection;

    public showComments = false;
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public likesCount = 0;
    public dislikesCount = 0;
    public likeColor = 'primary';
    public dislikeColor = 'primary';

    public showShare = false;
    public email = "";


    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private postService: PostService,
        private likeService: LikeService,
        private dislikeService: DisLikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private mainThreadComponent: MainThreadComponent,
        private dialog: MatDialog
    ) { }

    public ngOnInit(): void {
        this.registerHub();
        this.reactionsCalculate();
        //this.cloneNode
    }

    public registerHub() {
        this.postHub.on('UpdatePost', (updatedPost: Post) => {
            if (updatedPost) {
                this.updatePost(updatedPost);
            }
        });

        this.commentHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/comment').build();
        this.commentHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub.on('NewComment', (NewComment: Comment) => {
            if (NewComment) {
                this.addNewComment(NewComment);
            }
        });

        this.commentHub.on('DeleteComment', (commentId: number) => {
            if (commentId) {
                this.deleteComment(commentId);
            }
        });

        this.commentHub.on('UpdateComment', (comment: Comment) => {
            if (comment) {
                this.updateComment(comment);
            }
        });

        this.reactionHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/reaction').build();
        this.reactionHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.reactionHub.on('PostReactions', (response) => {
            if (response) {
                this.updatePostReactions(response);
            }
        });

        this.reactionHub.on('CommentReactions', (response) => {
            if (response) {
                this.updateCommentReactions(response);
            }
        });

    }

    public updateCommentReactions(response: any) {
        console.log("post");
        var index = this.post.comments.findIndex(comment => comment.id === response.commentId);
        if (index > -1) {
            this.post.comments[index].reactions = response.reactions;
        }
    }

    public updatePostReactions(response: { postId: number; reactions: Reaction[]; }) {
        if (response.postId === this.post.id) {
            this.post.reactions = response.reactions;
            this.reactionsCalculate();
        }
    }

    public updateComment(updatedComment: Comment) {
        if (this.post.id === updatedComment.postId) {
            var index = this.post.comments.findIndex(comment => comment.id === updatedComment.id);
            if (index > -1) {
                this.post.comments[index] = updatedComment;
            }
        }
    }

    public updateCommentEventHandler(updatedComment: Comment) {
        this.updateComment(updatedComment);
    }

    public deleteComment(commentId: number) {
        this.post.comments = this.post.comments.filter(comment => comment.id != commentId);
    }

    public addNewComment(NewComment: Comment) {
        if (!this.post.comments.some((x) => x.id === NewComment.id) && this.post.id === NewComment.postId) {
            this.post.comments = this.sortCommentArray(this.post.comments.concat(NewComment));
        }
    }

    public updatePost(updatedPost: Post) {
        if (updatedPost.id === this.post.id) {
            this.post = updatedPost;
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public toggleShare() {
        this.showShare = !this.showShare;
    }

    public sharePostMail() {
        var sharePost = {} as SharePost;
        sharePost.postId = this.post.id;
        sharePost.email = this.email;

        console.log(sharePost);
        this.postService
            .sharePost(sharePost)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.snackBarService.showUsualMessage("post sent to " + sharePost.email);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        this.showShare = false;
    }

    public reactionsCalculate() {
        this.likesCount = this.post.reactions.filter(reaction => reaction.isReaction == true).length;
        this.dislikesCount = this.post.reactions.filter(reaction => reaction.isReaction == false).length;

        this.likeColor = 'primary';
        this.dislikeColor = 'primary';

        if (this.currentUser != null) {
            var myReaction = this.post.reactions.filter((x) => x.user.id === this.currentUser.id)[0];

            if (myReaction != null) {
                if (myReaction.isReaction)
                    this.likeColor = 'accent';
                if (!myReaction.isReaction)
                    this.dislikeColor = 'warn';
            }
        }
    }

    public showStatistics() {
        const dialog = this.dialog.open(StatisticsDialogComponent, {
            data: { reactions: this.post.reactions },
            minWidth: 250,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.dislikeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.dislikeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.addNewComment(resp.body);
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deleteCommentEventHandler(emitedValue: number) {
        this.deleteComment(emitedValue);
    }

    public canEdit() {
        if (this.currentUser != null && this.post.author.id == this.currentUser.id)
            return true;

        return false;
    }

    public editPost() {
        const dialog = this.dialog.open(EditPostDialogComponent, {
            data: { post: this.post },
            minWidth: 600,
            maxWidth: 600,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });

        dialog
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((resp) => {
                if (resp) {
                    this.updatePost(resp.body);
                }
            });
    }

    public deletePost() {
        this.postService
            .deletePost(this.post.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.mainThreadComponent.deletePost(this.post.id);
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
