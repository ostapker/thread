﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class RenameIsLikeToIsReaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDisLike",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsLike",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsDisLike",
                table: "CommentReactions");

            migrationBuilder.DropColumn(
                name: "IsLike",
                table: "CommentReactions");

            migrationBuilder.AddColumn<bool>(
                name: "IsReaction",
                table: "PostReactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsReaction",
                table: "CommentReactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsReaction", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 9, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(6027), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(6031), 18 },
                    { 2, 18, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5539), true, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5554), 6 },
                    { 3, 15, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5586), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5591), 10 },
                    { 4, 19, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5613), true, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5617), 8 },
                    { 5, 10, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5641), true, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5646), 1 },
                    { 6, 6, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5668), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5672), 16 },
                    { 7, 10, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5692), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5696), 10 },
                    { 8, 1, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5729), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5734), 3 },
                    { 9, 4, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5756), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5760), 18 },
                    { 10, 20, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5782), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5786), 7 },
                    { 1, 13, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(4479), true, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5131), 4 },
                    { 12, 3, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5832), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5836), 1 },
                    { 13, 7, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5857), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5862), 12 },
                    { 14, 16, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5882), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5886), 18 },
                    { 15, 4, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5906), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5910), 8 },
                    { 16, 15, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5930), true, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5934), 8 },
                    { 17, 12, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5954), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5958), 4 },
                    { 18, 11, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5978), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5982), 5 },
                    { 19, 1, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(6002), false, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(6007), 6 },
                    { 11, 11, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5806), true, new DateTime(2020, 6, 18, 14, 9, 11, 348, DateTimeKind.Local).AddTicks(5811), 21 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Eveniet eos ratione earum ut nisi mollitia.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(6103), 1, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(6690) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Asperiores quis voluptates quisquam pariatur sit.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7234), 13, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7249) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Enim atque eos dolor repellendus corrupti tenetur eum velit sed.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7339), 10, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7345) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Eum neque quia porro minus quos ipsa.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7407), 19, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7412) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Vel qui enim minus consequatur sequi earum sit.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7471), 9, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7476) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Optio dolores possimus soluta ea blanditiis.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7528), 11, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7533) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Est dolore sit iure.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7617), 15, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7623) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Dolores animi rem error accusamus ex.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7677), 7, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7683) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Rem facere ratione cum totam aut et illo eius.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7745), 1, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7750) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Eaque quis eaque omnis perspiciatis qui sed.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7804), 8, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7809) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Ad veritatis possimus consequatur quam ut eos excepturi nisi autem.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7871), 18, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7877) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Cumque eos fugiat porro molestias sit delectus ipsum quia quisquam.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7939), 2, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(7944) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Et aut voluptas fuga.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8031), 16, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8037) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, "Unde deleniti dicta sint expedita soluta.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8091), new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8096) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Temporibus et eos qui atque laboriosam fuga impedit.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8166), 13, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8172) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Velit facere tenetur nisi non quo.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8221), 17, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8227) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Amet quisquam id.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8265), 10, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8270) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Modi ab sint veritatis dolorem.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8316), 16, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8322) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Sed et eligendi tenetur quod explicabo.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8412), 13, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8418) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Doloremque eum asperiores quibusdam cupiditate maxime doloribus.", new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8476), 10, new DateTime(2020, 6, 18, 14, 9, 11, 334, DateTimeKind.Local).AddTicks(8481) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 148, DateTimeKind.Local).AddTicks(7942), "https://s3.amazonaws.com/uifaces/faces/twitter/gregsqueeb/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(1546) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2088), "https://s3.amazonaws.com/uifaces/faces/twitter/buddhasource/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2112) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2142), "https://s3.amazonaws.com/uifaces/faces/twitter/kostaspt/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2166), "https://s3.amazonaws.com/uifaces/faces/twitter/aka_james/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2170) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2190), "https://s3.amazonaws.com/uifaces/faces/twitter/ralph_lam/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2195) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2211), "https://s3.amazonaws.com/uifaces/faces/twitter/oskamaya/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2216) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2233), "https://s3.amazonaws.com/uifaces/faces/twitter/kylefoundry/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2237) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2255), "https://s3.amazonaws.com/uifaces/faces/twitter/abotap/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2259) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2277), "https://s3.amazonaws.com/uifaces/faces/twitter/laasli/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2298), "https://s3.amazonaws.com/uifaces/faces/twitter/lu4sh1i/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2303) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2320), "https://s3.amazonaws.com/uifaces/faces/twitter/thomasschrijer/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2324) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2341), "https://s3.amazonaws.com/uifaces/faces/twitter/normanbox/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2361), "https://s3.amazonaws.com/uifaces/faces/twitter/hai_ninh_nguyen/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2366) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2383), "https://s3.amazonaws.com/uifaces/faces/twitter/low_res/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2387) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2404), "https://s3.amazonaws.com/uifaces/faces/twitter/jackiesaik/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2408) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2425), "https://s3.amazonaws.com/uifaces/faces/twitter/abdots/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2446), "https://s3.amazonaws.com/uifaces/faces/twitter/dixchen/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2450) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2466), "https://s3.amazonaws.com/uifaces/faces/twitter/dhrubo/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2471) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2486), "https://s3.amazonaws.com/uifaces/faces/twitter/stephcoue/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2507), "https://s3.amazonaws.com/uifaces/faces/twitter/fran_mchamy/128.jpg", new DateTime(2020, 6, 18, 14, 9, 11, 149, DateTimeKind.Local).AddTicks(2511) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(8083), "https://picsum.photos/640/480/?image=623", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(8735) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(8976), "https://picsum.photos/640/480/?image=723", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9001) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9025), "https://picsum.photos/640/480/?image=5", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9030) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9048), "https://picsum.photos/640/480/?image=960", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9052) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9070), "https://picsum.photos/640/480/?image=888", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9074) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9090), "https://picsum.photos/640/480/?image=699", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9094) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9111), "https://picsum.photos/640/480/?image=377", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9115) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9199), "https://picsum.photos/640/480/?image=910", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9203) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9221), "https://picsum.photos/640/480/?image=177", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9225) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9241), new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9245) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9261), "https://picsum.photos/640/480/?image=931", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9265) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9282), "https://picsum.photos/640/480/?image=1066", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9286) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9302), "https://picsum.photos/640/480/?image=355", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9306) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9323), "https://picsum.photos/640/480/?image=337", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9327) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9343), "https://picsum.photos/640/480/?image=439", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9347) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9363), "https://picsum.photos/640/480/?image=616", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9367) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9383), "https://picsum.photos/640/480/?image=701", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9387) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9404), "https://picsum.photos/640/480/?image=282", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9408) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9424), "https://picsum.photos/640/480/?image=33", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9445), "https://picsum.photos/640/480/?image=901", new DateTime(2020, 6, 18, 14, 9, 11, 154, DateTimeKind.Local).AddTicks(9449) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsReaction", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1891), true, 14, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1895), 12 },
                    { 11, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1913), true, 3, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1917), 20 },
                    { 12, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1936), true, 13, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1940), 7 },
                    { 13, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1959), false, 4, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1963), 10 },
                    { 15, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2006), false, 17, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2010), 13 },
                    { 18, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2073), true, 4, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2077), 16 },
                    { 16, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2029), false, 8, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2033), 6 },
                    { 17, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2051), true, 3, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2055), 11 },
                    { 9, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1868), true, 6, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1872), 15 },
                    { 20, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2117), false, 5, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2121), 21 },
                    { 19, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2095), true, 6, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(2099), 7 },
                    { 14, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1981), true, 12, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1985), 7 },
                    { 8, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1845), false, 12, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1849), 19 },
                    { 3, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1724), false, 3, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1729), 4 },
                    { 6, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1800), true, 6, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1804), 9 },
                    { 5, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1776), false, 13, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1780), 1 },
                    { 1, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(637), true, 11, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1200), 3 },
                    { 4, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1750), false, 19, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1754), 17 },
                    { 2, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1679), true, 10, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1692), 20 },
                    { 7, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1822), true, 10, new DateTime(2020, 6, 18, 14, 9, 11, 342, DateTimeKind.Local).AddTicks(1826), 17 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "labore", new DateTime(2020, 6, 18, 14, 9, 11, 327, DateTimeKind.Local).AddTicks(4574), 37, new DateTime(2020, 6, 18, 14, 9, 11, 327, DateTimeKind.Local).AddTicks(5702) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "vel", new DateTime(2020, 6, 18, 14, 9, 11, 327, DateTimeKind.Local).AddTicks(6699), 28, new DateTime(2020, 6, 18, 14, 9, 11, 327, DateTimeKind.Local).AddTicks(6731) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "quia", new DateTime(2020, 6, 18, 14, 9, 11, 327, DateTimeKind.Local).AddTicks(6816), 30, new DateTime(2020, 6, 18, 14, 9, 11, 327, DateTimeKind.Local).AddTicks(6825) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 6, @"Inventore eaque harum eos voluptas nemo blanditiis.
Est et unde eum voluptatem minima veniam dolore iure omnis.
In qui mollitia nesciunt non occaecati debitis cumque odio est.
Error aut saepe ea sequi nulla doloremque id consequatur aut.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4137), new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4160) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "nesciunt", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4219), 34, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4226) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "nihil", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4259), 39, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4264) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "ab", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4294), 32, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(4299) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Cumque et rerum id perferendis voluptas cumque et iusto itaque. Consequuntur iusto eius est tenetur dolores officiis iste eum. Labore sed voluptas. Quas omnis eum sint provident non quidem eligendi delectus voluptas.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(5718), 23, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(5733) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 11, "est", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(5782), new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(5787) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Quae magnam in quas et.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6509), 29, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 2, "Quia beatae debitis.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6581), new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6587) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "id", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6666), 31, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6672) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Consequatur modi iusto totam necessitatibus cupiditate doloremque itaque sit fuga. Quo maiores quos sint nihil repellat quia in nihil. Voluptas molestiae laborum. Et quae aut.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6827), 32, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6833) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Itaque aliquid eaque doloremque eos aperiam voluptas ut.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6894), 22, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(6900) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Neque voluptatum qui id. Excepturi molestiae voluptatum non. Neque autem dolorem autem voluptatem deleniti tempora ut consequatur. Incidunt est harum qui. Veniam delectus ea dolores ea qui.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7096), 39, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7103) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Reprehenderit praesentium sed voluptate libero a neque aut ut officiis.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7175), 25, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7181) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Quo nesciunt ut ut voluptatibus iure.
Magni sed eveniet perspiciatis quasi reiciendis similique tempora tempora.
Nihil ducimus harum sit voluptatem itaque suscipit veniam et omnis.
Culpa magnam delectus.
Et architecto occaecati molestiae aperiam.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7442), 30, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7449) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Dicta molestias quia rerum sit.
Enim facilis veniam saepe non odit.
Vel modi error veritatis repudiandae et.
Omnis et recusandae labore qui et quae et.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7638), 24, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7645) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "et", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7677), 22, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7682) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Qui rerum voluptatem dolor. Ut architecto explicabo molestias libero nihil sed totam iusto molestias. Quibusdam dolor eius in aut iusto minima qui excepturi qui. Saepe nemo voluptatem porro officiis autem aliquid nam doloremque.", new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7884), 27, new DateTime(2020, 6, 18, 14, 9, 11, 328, DateTimeKind.Local).AddTicks(7891) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 18, 14, 9, 11, 184, DateTimeKind.Local).AddTicks(8627), "Arno_Dach@gmail.com", "UIBLOP5uGBuC+3Bu+8KhgE4plPOXSJ9u9LtwS3NS2bU=", "+gXAAVDy3v+WXf18IpSTqOn+h2X/mUMIOAjvOu3Rbww=", new DateTime(2020, 6, 18, 14, 9, 11, 184, DateTimeKind.Local).AddTicks(9332), "Esteban.Barrows73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 18, 14, 9, 11, 194, DateTimeKind.Local).AddTicks(3121), "Macie.OConnell@hotmail.com", "3eSaoz1L/yValHM5lnGEYx1YOliCJfMkFnEDfLg5ACo=", "5EyXJ97xwh8txTPFoJ/141hBKDMTh4r5KozTgDqmBOs=", new DateTime(2020, 6, 18, 14, 9, 11, 194, DateTimeKind.Local).AddTicks(3209), "Hettie.Jenkins" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 18, 14, 9, 11, 200, DateTimeKind.Local).AddTicks(6571), "Meaghan96@yahoo.com", "j0HRkTksfGyv0pYJWRrJtJWJmiHRRwtRVrvavZ+m2YY=", "chi75Thvf8XgT1mRdXqML22L2KjZvyW6eIfBhwuprAI=", new DateTime(2020, 6, 18, 14, 9, 11, 200, DateTimeKind.Local).AddTicks(6586), "Katarina2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 207, DateTimeKind.Local).AddTicks(1386), "Americo.Beer5@yahoo.com", "UMJmoM0+qbUOjdo6M0DlWiqS3QjRnJ+uxwIkvRkTDUU=", "kqit2dk5gVsc5KQxEfjqNVyHQLMTHUT5/sknqxdDzjQ=", new DateTime(2020, 6, 18, 14, 9, 11, 207, DateTimeKind.Local).AddTicks(1459), "Janick.Yost87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 18, 14, 9, 11, 214, DateTimeKind.Local).AddTicks(1276), "Hipolito41@yahoo.com", "bKFHp/hBx8kVbbY2CSRZEkrstvXEy5Yg+Yh7RRglC4U=", "aM6HDYTIqEgPsp8eC00uR4DXjt7SyeARRyknraEIHOY=", new DateTime(2020, 6, 18, 14, 9, 11, 214, DateTimeKind.Local).AddTicks(1318), "Cielo.Hills37" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 18, 14, 9, 11, 220, DateTimeKind.Local).AddTicks(7274), "Kaia_Kohler@gmail.com", "uxgt5k+CVpb2WpE04+i44BgT4mSEXbeUdrO6UMXDJN4=", "u7AltxMF8jD7qX6Rlpnptrenj2IpIeVkK+0SbkM5D1E=", new DateTime(2020, 6, 18, 14, 9, 11, 220, DateTimeKind.Local).AddTicks(7410), "Joesph.Weber21" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 18, 14, 9, 11, 227, DateTimeKind.Local).AddTicks(2439), "Edythe_MacGyver41@yahoo.com", "Zw7WoW7+NVIlWox2GPubyMsiPLlZGZ0bPq5AhOV1XZw=", "wIrtMTWlpOrhxmLKx7UZOVMbv8lGuE+wNgkkRpqqke4=", new DateTime(2020, 6, 18, 14, 9, 11, 227, DateTimeKind.Local).AddTicks(2484), "Kari.DAmore58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 18, 14, 9, 11, 233, DateTimeKind.Local).AddTicks(5492), "Adonis73@gmail.com", "7ZhhyzPf6OcZL25NFBrSS6xGxVZVDuG+bVTRD+RDK+U=", "c+babRumj+Op8F1w5ZUjDDKP3TiNRDkh6yt2OS9KOOs=", new DateTime(2020, 6, 18, 14, 9, 11, 233, DateTimeKind.Local).AddTicks(5505), "Watson46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 18, 14, 9, 11, 239, DateTimeKind.Local).AddTicks(8405), "Retha_Boyer44@gmail.com", "a7eq5tB8Caf95DsdTKeGqH2AFF4JngOb7pGuD0mtpbY=", "Ekm8my4+iGZmaINBmy+jvSEcXFtyw2zv46iuFp2Rn/Q=", new DateTime(2020, 6, 18, 14, 9, 11, 239, DateTimeKind.Local).AddTicks(8458), "Glennie_Johnson" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 18, 14, 9, 11, 246, DateTimeKind.Local).AddTicks(5819), "Ruthe_Ortiz46@yahoo.com", "rUxZk2Z2rEINPB80UlfuQUdfE03DyFSz6he65cVj3Qs=", "wLWmGXQs5gVpK99CNDEP/W6/PkcpKwffAphNa3RDme8=", new DateTime(2020, 6, 18, 14, 9, 11, 246, DateTimeKind.Local).AddTicks(5855), "Myriam_Kuhn39" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 18, 14, 9, 11, 252, DateTimeKind.Local).AddTicks(8781), "Hannah.White44@hotmail.com", "JqvB3eZ+hOpG5QTlmvSrR/RrdwVjzM8ksb7aPw1UHNg=", "jk8eNJbSeUHci3wvjwVi50ROOm+eQPYqW1uL6WPNhgU=", new DateTime(2020, 6, 18, 14, 9, 11, 252, DateTimeKind.Local).AddTicks(8838), "Saige86" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 18, 14, 9, 11, 259, DateTimeKind.Local).AddTicks(3660), "Marlon_Howell87@hotmail.com", "s2Ht6qPZ7WcQKWxFj5YUzxXhbf6nzFTZQfXQsk9Luds=", "yp5X0AqReqwPadsTMB/e4llGgGcPAwu0nwscTjRilHM=", new DateTime(2020, 6, 18, 14, 9, 11, 259, DateTimeKind.Local).AddTicks(3697), "Elissa65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 18, 14, 9, 11, 265, DateTimeKind.Local).AddTicks(7877), "Kailee_Pouros@hotmail.com", "JYy4/75PuNkkh45ofLRuAtifvMEwqUbDg5IweCZGGXU=", "vW5ng8KlShLXBgHWWi5lOG8WRvHIqWtH7J6ogCRH+Zg=", new DateTime(2020, 6, 18, 14, 9, 11, 265, DateTimeKind.Local).AddTicks(7921), "Dusty20" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 18, 14, 9, 11, 272, DateTimeKind.Local).AddTicks(9056), "Malcolm.Gutkowski55@gmail.com", "TMzSpowFZScCYWrUHes6xM7Cwa1gcgYxjj4HP9e2u8Y=", "/pyBVv8CrdZ1cXS2D1jG5AE+enbc1QoWh092VZx7IkQ=", new DateTime(2020, 6, 18, 14, 9, 11, 272, DateTimeKind.Local).AddTicks(9123), "Raphael_Schaefer40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 18, 14, 9, 11, 279, DateTimeKind.Local).AddTicks(5478), "Delia.OKon@hotmail.com", "5KTTuWjMtYZwfg1tfCRaSVXrOEq68exl74hwOz0RgqM=", "eHZ5UuIMc01HX8irvloHVau57mva4MeZjby8bO/uYJ0=", new DateTime(2020, 6, 18, 14, 9, 11, 279, DateTimeKind.Local).AddTicks(5512), "Glennie95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 18, 14, 9, 11, 285, DateTimeKind.Local).AddTicks(9577), "Laisha.Bradtke44@yahoo.com", "QTLZX9ubs0SqzWT7JqKJjakyxuc/AtNlLoLyJnKNtwc=", "b5d2doJ/KeUVkPdHn2JOCRLSjBHVPvEAXeXT9+BApr0=", new DateTime(2020, 6, 18, 14, 9, 11, 285, DateTimeKind.Local).AddTicks(9630), "Davonte.Stamm" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 18, 14, 9, 11, 292, DateTimeKind.Local).AddTicks(9410), "Johnathan41@gmail.com", "NgVFPT6CKoejO58f/7PkvYxVPLT84611oClQraLjKYM=", "hj1yhs7eLEqhGGBo/SyxbqINzkUmKLgRHBfPMkoVLB0=", new DateTime(2020, 6, 18, 14, 9, 11, 292, DateTimeKind.Local).AddTicks(9472), "Houston_Littel78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 18, 14, 9, 11, 299, DateTimeKind.Local).AddTicks(2563), "Abigayle14@gmail.com", "xeSVNknOUcQEJA262rWG4RHtYTZnGfCpsevI7ZRan3k=", "8iPMfRkXtaN8XV+PbGMuzkKq+NQHqV47a2nlsXFTF4o=", new DateTime(2020, 6, 18, 14, 9, 11, 299, DateTimeKind.Local).AddTicks(2594), "Robert_Homenick" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 18, 14, 9, 11, 305, DateTimeKind.Local).AddTicks(6745), "Lisette.Krajcik@gmail.com", "EC07TEZIrx5ppWreohY+GtYTu5jJZyfDFUTyUYyOdyY=", "vmWmwCrHgrFrEWwufuNquBwUOkOK8hMpjBmW51Q+uKg=", new DateTime(2020, 6, 18, 14, 9, 11, 305, DateTimeKind.Local).AddTicks(6805), "Dorcas.Yundt41" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 18, 14, 9, 11, 312, DateTimeKind.Local).AddTicks(2243), "Moses_Beier@gmail.com", "4ZjKOg6qeafqu7uXl5p6ZVTlJ5gRaN+33MUrql+3byU=", "ydMedPxZrkQ6czHWGZXC/zZE53rfGPsfeT2d2Lr4tGc=", new DateTime(2020, 6, 18, 14, 9, 11, 312, DateTimeKind.Local).AddTicks(2283), "Casimir33" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 18, 14, 9, 11, 318, DateTimeKind.Local).AddTicks(6345), "vdxtzmz44T5cibg5EQdSo8DRV0c72PmXWTSMaECdhQY=", "J2uXll4DW5XGDqXyEIiZltUk8qhP635kPBo2bLnuiQA=", new DateTime(2020, 6, 18, 14, 9, 11, 318, DateTimeKind.Local).AddTicks(6345) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsReaction",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsReaction",
                table: "CommentReactions");

            migrationBuilder.AddColumn<bool>(
                name: "IsDisLike",
                table: "PostReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsLike",
                table: "PostReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDisLike",
                table: "CommentReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsLike",
                table: "CommentReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsDisLike", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 19, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5784), true, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5788), 19 },
                    { 2, 5, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5288), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5302), 14 },
                    { 3, 8, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5333), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5338), 5 },
                    { 4, 2, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5360), false, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5364), 5 },
                    { 5, 7, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5385), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5389), 20 },
                    { 6, 3, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5410), false, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5414), 19 },
                    { 7, 6, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5472), false, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5476), 1 },
                    { 8, 12, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5499), false, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5502), 11 },
                    { 9, 20, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5523), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5526), 10 },
                    { 10, 17, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5547), false, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5550), 18 },
                    { 1, 6, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(4417), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(4813), 20 },
                    { 12, 20, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5594), false, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5597), 1 },
                    { 13, 13, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5618), false, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5622), 12 },
                    { 14, 9, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5642), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5646), 20 },
                    { 15, 2, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5665), false, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5668), 10 },
                    { 16, 2, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5688), true, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5692), 19 },
                    { 17, 6, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5712), false, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5715), 12 },
                    { 18, 4, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5738), true, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5742), 21 },
                    { 19, 9, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5761), true, false, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5765), 19 },
                    { 11, 17, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5570), false, true, new DateTime(2020, 6, 17, 21, 24, 36, 891, DateTimeKind.Local).AddTicks(5574), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Consequatur unde voluptas voluptate atque consequatur facilis odit.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(8609), 17, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(8976) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Accusamus molestias consequatur quia sint distinctio eos eius.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9415), 15, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9428) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Et error est ut iure amet et.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9497), 2, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9502) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Enim est optio expedita consectetur.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9562), 1, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9567) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Dolorem cupiditate tempore blanditiis est.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9654), 19, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9659) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Iusto et nulla expedita omnis.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9705), 1, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Deleniti voluptas quisquam qui dolorum praesentium id officiis omnis.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9769), 12, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9773) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Fugit totam quia deleniti aut non vel non.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9824), 13, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9827) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Id sapiente inventore placeat et fugit nihil vel porro.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9883), 19, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9887) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Molestiae praesentium odio libero.", new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9925), 19, new DateTime(2020, 6, 17, 21, 24, 36, 880, DateTimeKind.Local).AddTicks(9929) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Optio iure qui quia numquam ut maxime commodi perspiciatis.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(16), 5, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(21) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Et et molestiae maiores dolorum natus impedit quia omnis.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(73), 11, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(77) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Sapiente adipisci nostrum eveniet voluptatem tempore quaerat tempore nostrum.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(128), 19, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(132) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 19, "Laboriosam possimus nulla dolor est placeat.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(176), new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(179) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Voluptatibus ab enim quas rem qui aut.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(227), 15, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(231) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Ea eos iste.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(299), 1, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(304) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Nulla laboriosam officiis consequatur ut dolores placeat rerum.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(353), 13, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(357) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Quo non atque aut blanditiis suscipit laboriosam reiciendis ratione.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(408), 8, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(412) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Pariatur ratione cum unde vitae natus deserunt.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(458), 11, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(462) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Aspernatur nulla velit consequatur qui sapiente atque corporis commodi.", new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(514), 17, new DateTime(2020, 6, 17, 21, 24, 36, 881, DateTimeKind.Local).AddTicks(518) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 713, DateTimeKind.Local).AddTicks(9197), "https://s3.amazonaws.com/uifaces/faces/twitter/nepdud/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2413) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2889), "https://s3.amazonaws.com/uifaces/faces/twitter/eduardostuart/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2908) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2934), "https://s3.amazonaws.com/uifaces/faces/twitter/kurtinc/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2939) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2957), "https://s3.amazonaws.com/uifaces/faces/twitter/darylws/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2961) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2978), "https://s3.amazonaws.com/uifaces/faces/twitter/rmlewisuk/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2981) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(2997), "https://s3.amazonaws.com/uifaces/faces/twitter/michzen/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3001) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3016), "https://s3.amazonaws.com/uifaces/faces/twitter/aaronalfred/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3020) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3036), "https://s3.amazonaws.com/uifaces/faces/twitter/johannesneu/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3040) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3056), "https://s3.amazonaws.com/uifaces/faces/twitter/prinzadi/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3060) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3076), "https://s3.amazonaws.com/uifaces/faces/twitter/praveen_vijaya/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3079) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3095), "https://s3.amazonaws.com/uifaces/faces/twitter/xripunov/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3099) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3115), "https://s3.amazonaws.com/uifaces/faces/twitter/markolschesky/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3118) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3134), "https://s3.amazonaws.com/uifaces/faces/twitter/timmillwood/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3138) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3154), "https://s3.amazonaws.com/uifaces/faces/twitter/conspirator/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3158) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3174), "https://s3.amazonaws.com/uifaces/faces/twitter/sharvin/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3178) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3193), "https://s3.amazonaws.com/uifaces/faces/twitter/ateneupopular/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3197) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3213), "https://s3.amazonaws.com/uifaces/faces/twitter/ionuss/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3217) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3233), "https://s3.amazonaws.com/uifaces/faces/twitter/zforrester/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3237) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3252), "https://s3.amazonaws.com/uifaces/faces/twitter/okansurreel/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3256) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3271), "https://s3.amazonaws.com/uifaces/faces/twitter/iamsteffen/128.jpg", new DateTime(2020, 6, 17, 21, 24, 36, 714, DateTimeKind.Local).AddTicks(3275) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(6790), "https://picsum.photos/640/480/?image=933", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7235) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7422), "https://picsum.photos/640/480/?image=177", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7445) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7467), "https://picsum.photos/640/480/?image=409", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7472) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7488), "https://picsum.photos/640/480/?image=855", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7491) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7506), "https://picsum.photos/640/480/?image=838", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7510) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7525), "https://picsum.photos/640/480/?image=529", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7529) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7544), "https://picsum.photos/640/480/?image=7", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7548) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7563), "https://picsum.photos/640/480/?image=501", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7567) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7582), "https://picsum.photos/640/480/?image=41", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7585) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7600), new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7604) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7618), "https://picsum.photos/640/480/?image=386", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7622) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7677), "https://picsum.photos/640/480/?image=707", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7682) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7699), "https://picsum.photos/640/480/?image=305", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7703) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7718), "https://picsum.photos/640/480/?image=680", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7721) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7736), "https://picsum.photos/640/480/?image=573", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7740) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7755), "https://picsum.photos/640/480/?image=321", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7759) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7774), "https://picsum.photos/640/480/?image=201", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7778) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7792), "https://picsum.photos/640/480/?image=109", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7796) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7811), "https://picsum.photos/640/480/?image=345", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7815) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7830), "https://picsum.photos/640/480/?image=436", new DateTime(2020, 6, 17, 21, 24, 36, 718, DateTimeKind.Local).AddTicks(7833) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsDisLike", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8086), true, false, 14, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8090), 2 },
                    { 11, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8110), false, true, 20, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8114), 6 },
                    { 12, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8134), true, true, 20, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8138), 14 },
                    { 13, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8157), true, false, 13, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8161), 12 },
                    { 15, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8206), true, false, 9, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8210), 14 },
                    { 18, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8275), true, true, 14, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8279), 6 },
                    { 16, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8229), true, true, 16, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8233), 18 },
                    { 17, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8252), true, true, 2, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8256), 2 },
                    { 9, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8062), false, true, 15, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8066), 19 },
                    { 20, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8321), false, true, 5, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8325), 10 },
                    { 19, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8298), false, false, 6, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8302), 18 },
                    { 14, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8183), true, false, 12, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8187), 21 },
                    { 8, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8039), true, true, 13, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8044), 18 },
                    { 3, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7914), false, true, 9, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7918), 15 },
                    { 6, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7990), true, false, 17, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7994), 19 },
                    { 5, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7965), false, false, 18, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7969), 9 },
                    { 1, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(6976), true, false, 6, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7373), 2 },
                    { 4, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7941), true, false, 18, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7945), 5 },
                    { 2, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7867), false, true, 5, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(7880), 12 },
                    { 7, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8015), false, false, 11, new DateTime(2020, 6, 17, 21, 24, 36, 886, DateTimeKind.Local).AddTicks(8019), 10 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "numquam", new DateTime(2020, 6, 17, 21, 24, 36, 875, DateTimeKind.Local).AddTicks(4930), 39, new DateTime(2020, 6, 17, 21, 24, 36, 875, DateTimeKind.Local).AddTicks(5349) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Ut ipsam magni dolorem reiciendis ea illo quia alias fugiat. Non nulla accusantium. Reiciendis ut quos ut ipsum minima aut cupiditate sed dolore. Labore sunt voluptas iure. Esse in illo et et qui et tempora enim animi. Ut nobis dolor explicabo nihil nemo.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(1508), 21, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(1532) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Fuga rerum est dolorem autem aut sint qui sed delectus.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2063), 38, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2076) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 14, "Temporibus autem quasi qui. Voluptatem sapiente sunt vero blanditiis. Earum sequi qui recusandae illo sunt maiores ut autem. Dignissimos corrupti aut in voluptas voluptate sequi quidem ut. Id similique ut repudiandae non sapiente voluptas odit.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2377), new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2384) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "placeat", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2418), 35, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2422) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "voluptas", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2482), 28, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2487) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Quae illum commodi ut rerum sapiente illum ut. Consequatur itaque ipsam est aut autem amet sed alias. Dolore aut dolor. Eligendi eligendi dolor nesciunt. Natus eaque delectus est facere odit est. Id similique quae alias voluptatem eos et at.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2673), 23, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2678) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "In fugiat sit vel ut placeat et est. Et odit et enim voluptate qui voluptas non. Et esse fuga qui delectus a explicabo. Quod ipsam minima natus. Quibusdam nemo vel voluptatibus sed. Repudiandae non ea qui inventore non sint itaque velit.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2883), 35, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2888) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 19, "aut", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2918), new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(2922) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Et asperiores accusamus modi quisquam ipsam.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3006), 21, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3012) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 9, "Reprehenderit enim quos.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3053), new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3058) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Aliquid quaerat deserunt error et.
Voluptatem eveniet excepturi.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3471), 21, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3483) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Iure et officiis placeat maiores sit natus consectetur.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3553), 38, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3558) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Illum cupiditate et nihil numquam nemo molestiae hic.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3658), 33, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3664) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "voluptas", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3692), 31, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3697) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Aut asperiores minima qui totam consequatur aut dolorum placeat. Quis dolores odit blanditiis et quia. Fugiat cupiditate reprehenderit consectetur ullam nihil nulla voluptatem voluptatem accusamus.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3830), 27, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3835) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "facilis", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3865), 31, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3870) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Ea asperiores molestias.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3939), 27, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(3944) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, @"Accusamus eos qui excepturi veritatis sed.
Qui molestias exercitationem consequatur harum quas nostrum.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(4024), 29, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(4029) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Natus qui quas autem.", new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(4071), 36, new DateTime(2020, 6, 17, 21, 24, 36, 876, DateTimeKind.Local).AddTicks(4076) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 17, 21, 24, 36, 746, DateTimeKind.Local).AddTicks(8892), "Kenneth_Heaney73@gmail.com", "ejWvtuuv0htoC9LqCLJ1DuwlZ7pMBZg4+LgF9LXimC8=", "EtOWXC5xwH9dnFW86VfWrORcpiuv+U6zmQBM9nMJKpw=", new DateTime(2020, 6, 17, 21, 24, 36, 746, DateTimeKind.Local).AddTicks(9352), "Patsy.Trantow" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 17, 21, 24, 36, 753, DateTimeKind.Local).AddTicks(5651), "Pablo.Wisoky67@yahoo.com", "V97Fi2vQYTXEcTvP11V161LHpiPcKUfGmoY8nVqD+VA=", "a8ZYZGKlZl2cWIpbayoAaD32somCfRtJIAjAAPu0RGs=", new DateTime(2020, 6, 17, 21, 24, 36, 753, DateTimeKind.Local).AddTicks(5696), "Amber.Sporer41" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 17, 21, 24, 36, 759, DateTimeKind.Local).AddTicks(8192), "Rahul.Treutel64@hotmail.com", "5A8DdpQn2NTaehUjvqRZn8V9pLjZz5eGviFr2ds983I=", "yvt5sAUz31e3id+HTg1XDMq+mgbP3Uu0sCvXbFst7Gg=", new DateTime(2020, 6, 17, 21, 24, 36, 759, DateTimeKind.Local).AddTicks(8214), "Dakota89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 765, DateTimeKind.Local).AddTicks(9268), "Adrianna.Dare10@hotmail.com", "hHjXi8E4NaYOjKyplwdoW+P3oNlEEUAT17Vh+nw8Siw=", "jDnXLEIlbnyCDIkfILIbn1NDp+3aHUOl5wkHN14DDqc=", new DateTime(2020, 6, 17, 21, 24, 36, 765, DateTimeKind.Local).AddTicks(9285), "Domingo_Bashirian97" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 17, 21, 24, 36, 772, DateTimeKind.Local).AddTicks(3148), "Thurman_Lehner@gmail.com", "1e/HMrubQcq89iHhy2gAlM4mFTfdrqFdjfs3sAEiU8U=", "Rw+dZ5X6Ba9Jjo+feu/xx8wvQOpp/727Nxh9Ma/mn5A=", new DateTime(2020, 6, 17, 21, 24, 36, 772, DateTimeKind.Local).AddTicks(3174), "Sabina_Stiedemann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 17, 21, 24, 36, 778, DateTimeKind.Local).AddTicks(3042), "Katharina.Brown@yahoo.com", "KLPNiTh3udmWj3dUH4NE7r2k9TmZgQ8JZs1JaWAcjXU=", "lHlaserzFgLXkH5agCfxd4daT4CmPwyo0Cr6yce0g3o=", new DateTime(2020, 6, 17, 21, 24, 36, 778, DateTimeKind.Local).AddTicks(3062), "Cecile.Prohaska93" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 17, 21, 24, 36, 784, DateTimeKind.Local).AddTicks(5018), "Jolie.Farrell82@gmail.com", "47HK2sHidVHAVaO7G4Er3ZjbV5zZgubWojX7JZG3EoE=", "ZBaaopOTENiRp5+SBeMLw1gZVrb73FarEXKfGzR9VBA=", new DateTime(2020, 6, 17, 21, 24, 36, 784, DateTimeKind.Local).AddTicks(5035), "Willis_Hansen72" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 17, 21, 24, 36, 790, DateTimeKind.Local).AddTicks(6470), "Sallie_Abshire@gmail.com", "U3qboYuYoqhPbodJQbRWO8vfS3Gd4w+IvYPt+jSE3To=", "5w/XP8+E6waqEossafhIHFcdK/eNDa7kxNXIHWBJROo=", new DateTime(2020, 6, 17, 21, 24, 36, 790, DateTimeKind.Local).AddTicks(6499), "Rhianna64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 17, 21, 24, 36, 796, DateTimeKind.Local).AddTicks(6416), "Jarrett31@yahoo.com", "TmFpO1Vd5HUYlVlicz3ruQXrPm354UKGwhzla1r/o4M=", "OfJCUL/YnK9P1bhdIpTWlFwxsFbU04ayCMt6pT2HB6Y=", new DateTime(2020, 6, 17, 21, 24, 36, 796, DateTimeKind.Local).AddTicks(6432), "Dandre_Mueller" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 17, 21, 24, 36, 803, DateTimeKind.Local).AddTicks(314), "Eveline0@yahoo.com", "Jeac7uUFVpRppgpbAt/M638vM4ooOobaPT24QVR6D04=", "XUKQOzbDuKBQu+y8PYIzMNcbUseFF8/b3v98rJnfnSI=", new DateTime(2020, 6, 17, 21, 24, 36, 803, DateTimeKind.Local).AddTicks(334), "Isobel.Osinski12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 17, 21, 24, 36, 808, DateTimeKind.Local).AddTicks(9719), "Fletcher.Balistreri81@hotmail.com", "GeXk+JOD8mdCCrCRdK21O98GZhhpZuBJlW727C560X4=", "2PMuzNCIa4TLi13FoFKQQfabkft/87qfNgjAn/H7DAc=", new DateTime(2020, 6, 17, 21, 24, 36, 808, DateTimeKind.Local).AddTicks(9731), "Savanah_Rogahn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 17, 21, 24, 36, 814, DateTimeKind.Local).AddTicks(9921), "Manuel.Jerde@yahoo.com", "somNaMYAxJI1s0/s41AMEN4AhYgk/JnfgMWAIu+EEQo=", "dNzGSjqOWhOCqsVq6KnF17aqmBJxiHTzGHy/13F4u30=", new DateTime(2020, 6, 17, 21, 24, 36, 814, DateTimeKind.Local).AddTicks(9936), "Mike27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 17, 21, 24, 36, 821, DateTimeKind.Local).AddTicks(5170), "Oswald.Schroeder63@gmail.com", "UXXArM+GoZqvj/k+BaqjJPF+fyKSUr1UUw20/ZsEVSI=", "ntciLi8idfgzco1seYk0by58TfLzlQfNzEU/NamTgUk=", new DateTime(2020, 6, 17, 21, 24, 36, 821, DateTimeKind.Local).AddTicks(5194), "Nyah.Murphy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 17, 21, 24, 36, 827, DateTimeKind.Local).AddTicks(4538), "Lura.Welch40@yahoo.com", "z4TQ8dMpXvnuEAByBxHZ5xdSBhKE1y9KyN2GrEsBOho=", "QnltcHgRx7kIu57nWVWKW+45VNDgiNqAtRLTu2ozk2M=", new DateTime(2020, 6, 17, 21, 24, 36, 827, DateTimeKind.Local).AddTicks(4552), "Earl_Kihn87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 17, 21, 24, 36, 833, DateTimeKind.Local).AddTicks(6834), "Caleigh27@yahoo.com", "SKyivl2seIgbnfxMfBQ/h2AxH3WR3+DLSdEN4Vw2q78=", "ilaUWF0TQB5+T9Yw8hniwfbohiwTZuaBS7U2KPaNiak=", new DateTime(2020, 6, 17, 21, 24, 36, 833, DateTimeKind.Local).AddTicks(6851), "Guillermo_Brakus" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 17, 21, 24, 36, 840, DateTimeKind.Local).AddTicks(445), "Dorothy.Feeney@hotmail.com", "VYicTPXK6BA9fWL+/0voIQl7SeYCDWOKCnT34odva8w=", "EA4QHr1xw+UzC94giTFZ2DldQ8SZWZX3VV677xDBuxw=", new DateTime(2020, 6, 17, 21, 24, 36, 840, DateTimeKind.Local).AddTicks(461), "Chesley89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 17, 21, 24, 36, 845, DateTimeKind.Local).AddTicks(9484), "Ceasar96@hotmail.com", "Rai3cMtPwzZtl+BLrhoHgqGMSP04vrRItxHLCiPMusU=", "yyOW5JWe3yY/g/Dkf0flcaBlYB2i4kJgBd5D7CKyH24=", new DateTime(2020, 6, 17, 21, 24, 36, 845, DateTimeKind.Local).AddTicks(9498), "Ryann_Hirthe29" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 17, 21, 24, 36, 852, DateTimeKind.Local).AddTicks(824), "Natasha4@hotmail.com", "+0JeEfh5DjGV4i6ssaGgkyulHfWsTqK1sSiatRbaTww=", "yMz02SqrbkhUeoEA1d3OXo3G5mfzOpbqAuG4eNufzrU=", new DateTime(2020, 6, 17, 21, 24, 36, 852, DateTimeKind.Local).AddTicks(845), "Laisha.Kessler" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 17, 21, 24, 36, 857, DateTimeKind.Local).AddTicks(8992), "Willa_Doyle17@yahoo.com", "vUvR4lKTQKJ5z3qypTGUGSrN4YSAFtXnxja6IfT6Rec=", "xvu8LwhXFo4rDGTuP7BZNEQrVJzuiP5n0uuxvmPmoqc=", new DateTime(2020, 6, 17, 21, 24, 36, 857, DateTimeKind.Local).AddTicks(9004), "Gaston_Paucek36" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 17, 21, 24, 36, 863, DateTimeKind.Local).AddTicks(8201), "Santos.Purdy@gmail.com", "HxazXf8fmSmRX9Ja8gMn7rBvPc0WtiO/rFSrTo941DQ=", "qg93WiRHWWFnCXdfHuaIf4dfe308tYfYSDP4ijekTa0=", new DateTime(2020, 6, 17, 21, 24, 36, 863, DateTimeKind.Local).AddTicks(8215), "Sarah.Senger3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 17, 21, 24, 36, 870, DateTimeKind.Local).AddTicks(1090), "XRn3zYdpayzvtmwDB2e4purFAaVVifma9V5DDMerz2E=", "oK8nOPYovYb/9jUAVjbCggtEPGS8Va21x0qr1T1tS78=", new DateTime(2020, 6, 17, 21, 24, 36, 870, DateTimeKind.Local).AddTicks(1090) });
        }
    }
}
