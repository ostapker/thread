﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class SharePostDTO
    {
        public int PostId { get; set; }
        public string Email { get; set; }
    }
}
