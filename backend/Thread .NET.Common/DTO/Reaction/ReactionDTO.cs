﻿using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Reaction
{
    public sealed class ReactionDTO
    {
        public bool IsReaction { get; set; }
        public UserDTO User { get; set; }
    }
}
