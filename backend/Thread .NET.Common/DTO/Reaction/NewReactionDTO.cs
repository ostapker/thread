﻿namespace Thread_.NET.Common.DTO.Reaction
{
    public sealed class NewReactionDTO
    {
        public int EntityId { get; set; }
        public bool IsReaction { get; set; }
        public int UserId { get; set; }
    }
}
