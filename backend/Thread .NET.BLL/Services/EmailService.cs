﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Thread_.NET.BLL.Services
{
    public sealed class EmailService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress("thread.bsa@gmail.com", "Thread Administration"),
                Subject = subject,
                Body = message,
                IsBodyHtml = true,
            };
            mailMessage.To.Add(email);

            using var client = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential("thread.bsa@gmail.com", "thread2020"),
                EnableSsl = true
            };
            await client.SendMailAsync(mailMessage);
        }
    }
}
