﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class ReactionService : BaseService
    {

        private readonly IHubContext<ReactionHub> _reactionHub;

        private readonly EmailService _emailService;

        public ReactionService(
                ThreadContext context, 
                IMapper mapper, 
                IHubContext<ReactionHub> reactionHub,
                EmailService emailService
            ) : base(context, mapper) {
            _reactionHub = reactionHub;
            _emailService = emailService;
        }


        public async Task<ICollection<ReactionDTO>> GetReactionsByPostId(int id)
        {
            return await _context.PostReactions
                                    .Include(pr => pr.User)
                                        .ThenInclude(u => u.Avatar)
                                    .Where(x => x.PostId == id)
                                    .Select(x => _mapper.Map<ReactionDTO>(x))
                                    .ToListAsync();
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likesDelete = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsReaction == true).ToList();
            var likesUpdate = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsReaction == false).ToList();

            await ChangePostReaction(likesDelete, likesUpdate, reaction);
        }

        public async Task DisLikePost(NewReactionDTO reaction)
        {
            var dislikesDelete = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsReaction == false).ToList();
            var dislikesUpdate = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsReaction == true).ToList();

            await ChangePostReaction(dislikesDelete, dislikesUpdate, reaction);
        }

        private async Task ChangePostReaction(List<PostReaction> deleteReactions, List<PostReaction> updateReactions, NewReactionDTO newReaction)
        {
            Task email = null;
            Task notify = null;
            var authorId = _context.Posts.Where(post => post.Id == newReaction.EntityId).Select(post => post.AuthorId).FirstOrDefault();
            var userLiked = _context.Users.Where(user => user.Id == newReaction.UserId).FirstOrDefault();
            var author = _context.Users.Where(user => user.Id == authorId).FirstOrDefault();

            if (deleteReactions.Any())
            {
                _context.PostReactions.RemoveRange(deleteReactions);
            }
            else if (updateReactions.Any())
            {
                updateReactions.ForEach(x => {
                    x.IsReaction = newReaction.IsReaction;
                    x.UpdatedAt = System.DateTime.Now;
                });
                _context.PostReactions.UpdateRange(updateReactions);

                if (newReaction.IsReaction)
                {
                    email = _emailService.SendEmailAsync(author.Email, "Thread Notification",
                        $"<b>{userLiked.UserName}</b> liked your post!");
                    notify = _reactionHub.Clients.All.SendAsync("LikeNotify", new { user = _mapper.Map<UserDTO>(userLiked), authorId });
                }
            }
            else
            {
                _context.PostReactions.Add(new PostReaction
                {
                    PostId = newReaction.EntityId,
                    IsReaction = newReaction.IsReaction,
                    UserId = newReaction.UserId
                });

                if (newReaction.IsReaction)
                {
                    email = _emailService.SendEmailAsync(author.Email, "Thread Notification",
                        $"<b>{userLiked.UserName}</b> liked your post!");
                    notify = _reactionHub.Clients.All.SendAsync("LikeNotify", new { user = _mapper.Map<UserDTO>(userLiked), authorId });
                }
            }
            await _context.SaveChangesAsync();

            await _reactionHub.Clients.All.SendAsync("PostReactions", new { reactions = await GetReactionsByPostId(newReaction.EntityId), postId = newReaction.EntityId });
            if (notify != null)
                await notify;

            if (email != null)
                await email;
            
        }



        public async Task<ICollection<ReactionDTO>> GetReactionsByCommentId(int id)
        {
            return await _context.CommentReactions
                                    .Include(pr => pr.User)
                                        .ThenInclude(u => u.Avatar)
                                    .Where(x => x.CommentId == id)
                                    .Select(x => _mapper.Map<ReactionDTO>(x))
                                    .ToListAsync();
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likesDelete = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsReaction == true).ToList();
            var likesUpdate = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsReaction == false).ToList();

            await ChangeCommentReaction(likesDelete, likesUpdate, reaction);
        }

        public async Task DisLikeComment(NewReactionDTO reaction)
        {
            var dislikesDelete = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsReaction == false).ToList();
            var dislikesUpdate = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsReaction == true).ToList();

            await ChangeCommentReaction(dislikesDelete, dislikesUpdate, reaction);
        }

        private async Task ChangeCommentReaction(List<CommentReaction> deleteReactions, List<CommentReaction> updateReactions, NewReactionDTO newReaction)
        {
            if (deleteReactions.Any())
            {
                _context.CommentReactions.RemoveRange(deleteReactions);
            }
            else if (updateReactions.Any())
            {
                updateReactions.ForEach(x => {
                    x.IsReaction = newReaction.IsReaction;
                    x.UpdatedAt = System.DateTime.Now;
                    });
                _context.CommentReactions.UpdateRange(updateReactions);
            }
            else
            {
                _context.CommentReactions.Add(new CommentReaction
                {
                    CommentId = newReaction.EntityId,
                    IsReaction = newReaction.IsReaction,
                    UserId = newReaction.UserId
                });
            }
            await _context.SaveChangesAsync();

            await _reactionHub.Clients.All.SendAsync("CommentReactions", new { reactions = await GetReactionsByCommentId(newReaction.EntityId), commentId = newReaction.EntityId });
        }
    }
}
