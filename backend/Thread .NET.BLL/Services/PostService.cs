﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        private readonly EmailService _emailService;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, EmailService emailService) : base(context, mapper)
        {
            _postHub = postHub;
            _emailService = emailService;
        }

        private IQueryable<Post> LoadPosts()
        {
            return _context.Posts
               .Include(post => post.Author)
                   .ThenInclude(author => author.Avatar)
               .Include(post => post.Preview)
               .Include(post => post.Reactions)
                   .ThenInclude(reaction => reaction.User)
                       .ThenInclude(u => u.Avatar)
               .Include(post => post.Comments)
                   .ThenInclude(comment => comment.Reactions)
                       .ThenInclude(reaction => reaction.User)
                           .ThenInclude(u => u.Avatar)
               .Include(post => post.Comments)
                   .ThenInclude(comment => comment.Author)
                       .ThenInclude(u => u.Avatar)
               .AsQueryable();
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await LoadPosts()
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await LoadPosts()
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePost(PostUpdateDTO postDto)
        {
            var postEntity = await GetPostByIdInternal(postDto.Id);
            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postDto.Id);
            }

            postEntity.Body = postDto.Body;
            var timeNow = DateTime.Now;

            postEntity.UpdatedAt = timeNow;

            if (!string.IsNullOrEmpty(postDto.PreviewImage))
            {
                if (postEntity.Preview == null)
                {
                    postEntity.Preview = new Image
                    {
                        URL = postDto.PreviewImage
                    };
                }
                else
                {
                    postEntity.Preview.URL = postDto.PreviewImage;
                    postEntity.Preview.UpdatedAt = timeNow;
                }
            }
            else
            {
                if (postEntity.Preview != null)
                {
                    _context.Images.Remove(postEntity.Preview);
                }
            }

            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();
            var postEntityDTO = _mapper.Map<PostDTO>(postEntity);
            await _postHub.Clients.All.SendAsync("UpdatePost", postEntityDTO);

            return postEntityDTO;
        }

        public async Task DeletePost(int postId)
        {
            var postEntity = await _context.Posts.FirstOrDefaultAsync(u => u.Id == postId);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            var comments = _context.Comments.Where(comment => comment.PostId == postId).ToList();

            foreach(var comment in comments)
            {
                var commentReactions = _context.CommentReactions.Where(commentReaction => commentReaction.CommentId == comment.Id).ToList();
                commentReactions.ForEach(commentReaction => _context.CommentReactions.Remove(commentReaction));
                _context.Comments.Remove(comment);
            }

            var postReactions = _context.PostReactions.Where(postReaction => postReaction.PostId == postId).ToList();
            postReactions.ForEach(postReaction => _context.PostReactions.Remove(postReaction));

            _context.Posts.Remove(postEntity);
            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("DeletePost", postEntity.Id);
        }

        public async Task SharePostEmail(int postId, string email)
        {
            var postEntity = await _context.Posts
                .Include(post => post.Author)
                   .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .FirstOrDefaultAsync(u => u.Id == postId);

            

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            await _emailService.SendEmailAsync(email, "Thread Post",
                        $"<div style='font-size:22px;'>" +
                        $"<div style='display:flex; flex-direction:row;width:100%;margin-bottom: 5px'>" +
                        $"<img   src='{ postEntity.Author.Avatar.URL}' alt='Avatar'>" +
                        $"<div style='margin:0'> {postEntity.Author.UserName}</div>" +
                        $"</div>" +
                        $"<img style='width: 90%'   src='{ postEntity.Preview.URL}' alt='Avatar'>" +
                        $"<p style='font-size:15px; margin:0;'>{postEntity.Body}</p>" +
                        $"</div>");
        }

        private async Task<Post> GetPostByIdInternal(int id)
        {
            return await LoadPosts()
                .FirstAsync(post => post.Id == id);
        }

        public async Task<PostDTO> GetPostById(int id)
        {
            return _mapper.Map<PostDTO>(await GetPostByIdInternal(id));
        }
    }
}
