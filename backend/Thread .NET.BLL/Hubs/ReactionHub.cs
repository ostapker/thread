﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class ReactionHub : Hub
    {
        public async Task PostReactions(ICollection<ReactionDTO> reactions, int postId)
        {
            await Clients.All.SendAsync("PostReactions", new { reactions, postId });
        }

        public async Task LikeNotify(UserDTO user, int authorId)
        {
            await Clients.All.SendAsync("LikeNotify", new { user, authorId });
        }

        public async Task CommentReactions(ICollection<ReactionDTO> reactions, int commentId)
        {
            await Clients.All.SendAsync("CommentReactions", new { reactions, commentId });
        }
    }
}
