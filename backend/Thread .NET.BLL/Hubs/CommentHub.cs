﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Comment;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class CommentHub : Hub
    {
        public async Task New(CommentDTO comment)
        {
            await Clients.All.SendAsync("NewComment", comment);
        }

        public async Task Update(CommentDTO comment)
        {
            await Clients.All.SendAsync("UpdateComment", comment);
        }

        public async Task Delete(int commentId)
        {
            await Clients.All.SendAsync("DeleteComment", commentId);
        }
    }
}
