﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;

using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly ReactionService _reactionService;

        public PostsController(PostService postService, ReactionService reactionService)
        {
            _postService = postService;
            _reactionService = reactionService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<ActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _reactionService.LikePost(reaction);
            return Ok();
        }

        [HttpPost("dislike")]
        public async Task<ActionResult> DisLikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _reactionService.DisLikePost(reaction);
            return Ok();
        }

        [HttpPost("share")]
        public async Task<ActionResult> SharePost(SharePostDTO sharePost)
        {
            //reaction.UserId = this.GetUserIdFromToken();
            await _postService.SharePostEmail(sharePost.PostId, sharePost.Email);
            return Ok();
        }


        [HttpPut]
        public async Task<ActionResult<PostDTO>> Put([FromBody] PostUpdateDTO user)
        {
            return Ok(await _postService.UpdatePost(user));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _postService.DeletePost(id);
            return NoContent();
        }

    }
}